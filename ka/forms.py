from django import forms
from .models import Comment1


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment1
        fields=['subject','comment']
