from django.shortcuts import get_object_or_404, redirect, render,reverse
from django.contrib.auth.models import User
from ka.models import City, Comment1, Customer, Order, Service_Category, Service_Man, Status
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.contrib.auth import authenticate, login, logout
import datetime
from .forms import CommentForm
from django.core.mail import EmailMessage

from .models import services_function,v,register_order,gallery,gallery_data,City,add_product,Service_Category,cart
from django.contrib import messages


def homepage(r):
    context={"gallery":gallery.objects.all().order_by("-id"),
    'data1':Service_Category.objects.all().order_by("-id"),}
    cats=Service_Category.objects.all().order_by("category")
    context["category"]=cats
    try:
        check=Customer.objects.filter(user__id=r.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=r.user.id)
            context["data"]=data
    except:
        pass      
    return render(r,"homepage1234.html",context)

# Create your views here.
def home(request):
    context={"gallery":gallery.objects.all().order_by("-id"),
             'data1':Service_Category.objects.all().order_by("-id"),
             }
    if request.method=="POST":
        un=request.POST["uname"]
        pwd=request.POST["Password"]
        em=request.POST["email"]
        con=request.POST["contact"]
        tp=request.POST["gender"]
        last=request.POST["last"]
        fname=request.POST["first"]
        usr=User.objects.create_user(un,em,pwd)
        usr.first_name=fname
        usr.last_name=last
        usr.save()

        if "image" in request.FILES:
            img=request.FILES["image"]

        reg=Customer(user=usr, contact=con,gender=tp,image=img)
        reg.save()  
        context["status"]="your registration successfully now you are ready to login" 
    try:
        check=Customer.objects.filter(user__id=request.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=request.user.id)
            context["data"]=data
    except:
        pass        
    return render(request,"home.html",context)
# Create your views here.
def new(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}

    if request.method=="POST":
        un=request.POST["uname"]
        pwd=request.POST["Password"]
        em=request.POST["email"]
        con=request.POST["contact"]
        tp=request.POST["gender"]
        last=request.POST["last"] 
        fname=request.POST["first"]    #
        dat = datetime.date.today()
        address=request.POST["address"]
        city=request.POST["city"]
        profile_image=request.FILES["profile_image"]
        dob=request.POST["dob"]
        service_name=request.POST["service_name"]
        image=request.FILES["image"]
        experience=request.POST["experience"]
        user = User.objects.create_user(email=em, username=un, password=pwd, first_name=fname,last_name=last)  
        user.save()
        vila=Service_Man(doj=dat,image=image,user=user,contact=con,address=address,gender=tp,city=city,profile_image=profile_image,dob=dob,service_name=service_name,experience=experience)
        vila.save()
        context['status']="your form has been submitted after we send email if you selected"
        return render(request,"new.html",context)
   
 
    return render(request,"new.html",context)

def check_user(request):
    if request.method=="GET":
        un= request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not Exists")

def user_login(r):
    if r.method=="POST":
        un=r.POST["username"]
        pwd=r.POST["password"]
        user=authenticate(username=un,password=pwd)
        if user:
            login(r,user)       
            if user.is_superuser:
                return HttpResponseRedirect("/adminpage")
            else:
                res= HttpResponseRedirect("/homepage") 
                if "rememberme" in r.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res 

        else:
            return render(r,"home.html",{"status":"Invalid username or Password"})

    return HttpResponse("form")            
def user_logout(r):
    logout(r)
    res= HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res    

def thepage(r):
    return render(r,"thepage.html")    
def adminpage(r):
    if r.user.is_superuser:
        context={ "gallery":gallery.objects.all().order_by("-id"),
                  'data':services_function.objects.all().order_by("-id"),
                  'data1':Service_Category.objects.all().order_by("-id"),
                  "gallery_data":gallery_data.objects.all().order_by("-id"),
                  "function_data":add_product.objects.all().order_by("-id"),
                  'city':City.objects.all().order_by("-id"),
                  }
                  
        #this is for add City event form
        try:       
            if r.method=="POST":
                nm=r.POST["city"]
                data0=City(city=nm)
                data0.save()
                return render(r,"adminpage.html",context)  
            
        except:
            pass 
        #this is for service event form
        try:       
            if r.method=="POST":
                nm=r.POST["sharukh"]
                bnm=r.POST["bn"]
                data=services_function(service_name=nm,description=bnm)
                data.save()
                return render(r,"adminpage.html",context)  
                 
        except:
            pass 
        #this is for gallery    mdhe some verity sathi            
        try:       
            if r.method=="POST":
                n=r.POST["sw"]
                b=r.POST["sw2"]
                img = r.FILES["pimg"]
                da=gallery(cat_name=n,cover_pic=img,description=b)
                da.save()
                return render(r,"adminpage.html",context)  
 
        except:
            pass        
        #inside the gallery product   verity relatete pic add sathi  
        try:       
            if r.method=="POST":
                n=r.POST["st1"]
                b=r.POST["st2"]
                o=r.FILES["img"]
                n1=gallery.objects.get(id=n)                
                da=gallery_data(thegallery=n1,image=o,detail=b)
                da.save()
                return render(r,"adminpage.html",context)  

        except:
            pass         
        #this is for gallery
        try:
                if r.method=="POST":
                    bnmd=r.POST["kaka"]
                    dataa=v(description=bnmd)
                    dataa.save() 
                    return render(r,"adminpage.html",context)  

        except:
                pass 
      #this for add product category
        try:
                if r.method=="POST":
                    name=r.POST["category"]
                    select=r.POST["total"]
                    price=r.POST["desc"]
                    pimg=r.FILES["pimg"]
                    datw=Service_Category(category=name,desc=price,total=select,image=pimg)
                    datw.save() 
                    return render(r,"adminpage.html",context)  

        except:
                pass 
            # tis is for add product
        try:
            if r.method == "POST":
                name=r.POST["name"]
                name02=r.POST["select"]
                name2=Service_Category.objects.get(id=name02)
                name3=r.POST["price"]
                name4=r.FILES["pimg"]
                name5=r.POST["sw25"]
                name6=r.POST["price1"]
                same=add_product(product_name=name,product_category=name2,product_price=name3,OLD_price=name6,product_image=name4,Product_details=name5)
                same.save()
        except:
            pass  
        return render(r,"adminpage.html",context)  
    
         
    else:
        return redirect('homepage')    

def editpage(r):
    if r.user.is_superuser:
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        pid=r.GET["pid"]
        product=gallery.objects.get(id=pid)
        context["product"]=product
        if r.method=='POST':
            desc=r.POST["desc"]
            pname=r.POST["pname"]
            product.cat_name=pname
            product.description=desc
            if "pimg" in r.FILES:
                pn=r.FILES["pimg"]
                product.cover_pic=pn
            product.save()    
            context["status"]="Changes Saved Success"
            context["id"]=pid
        return render(r,"edit.html",context)   
    else:
        return redirect('home')         

def galleryeditpage(r):
    if r.user.is_superuser:
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        cats=gallery.objects.all().order_by("cat_name")
        context["category"]=cats
        pid=r.GET["pid"]
        product=gallery_data.objects.get(id=pid)
        context["product"]=product
        if r.method=='POST':
            desc=r.POST["desc"]
            pname=r.POST["pcat"]
            n1=gallery.objects.get(id=pname)   
            product.thegallery=n1
            product.detail=desc
            if "pimg" in r.FILES:
                pn=r.FILES["pimg"]
                product.image=pn
            product.save()    
            context["status"]="Changes Saved Success"
            context["id"]=pid
        return render(r,"galleryedit.html",context)        
    else:
        return redirect('home') 
def productedit(r):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    data=Service_Category.objects.all()
    context["hope"]=data
    pid=r.GET["pid"]
    product=add_product.objects.get(id=pid)
    context["product"]=product 
    if r.method=="POST":
        pname1=r.POST["pname"]
        pname2=r.POST["price"]
        pname3=r.POST["desc"]
        pname4=r.POST["price1"]
        p=r.POST["select"]
        v=Service_Category.objects.get(id=p)
        product.product_category=v
        product.product_name=pname1
        product.product_price=pname2
        product.OLD_price=pname4
        product.Product_details=pname3
        if "pimg" in r.FILES:
            pname=r.FILES["pimg"]
            product.product_image=pname
        product.save()  
        context["status"]="Changes Saved Success"
        context["id"]=pid
    return render(r,"productedit.html",context)


def delete_productgallery(request):
    if request.user.is_superuser:
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        if "pid" in request.GET:
            pid = request.GET["pid"]
            prd = get_object_or_404(gallery, id=pid)
            context["product"] = prd
            if "action" in request.GET:
                prd.delete()
                context["status"] =str(prd.cat_name)+"removed successfully!!!"   
        return render(request,"deleteproduct.html",context)        
    else:
        return redirect('home')

def deleteNote(request):
    if request.user.is_superuser:    
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        if "pid" in request.GET:
            pid = request.GET["pid"]
            prd = get_object_or_404(gallery_data, id=pid)
            context["product"] = prd
            if "action" in request.GET:
                prd.delete()
                context["status"] =str(prd.thegallery.cat_name)+"removed successfully!!!"
        return render(request,"gallerydelete.html",context)  
    else:
        return redirect('home')

def deleteproduct(request):
    if request.user.is_superuser:    
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        if "pid" in request.GET:
            pid = request.GET["pid"]
            prd = get_object_or_404(add_product, id=pid)
            context["product"] = prd
            if "action" in request.GET:
                prd.delete()
                context["status"] =str(prd.product_name)+"removed successfully!!!"
        return render(request,"thefunctiondelete.html",context)  
    else:
        return redirect('home')
def aboutpage(r):
    context={"gallery":gallery.objects.all().order_by("-id"),
             'data1':Service_Category.objects.all().order_by("-id"),
             }
    try:
        check=Customer.objects.filter(user__id=r.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=r.user.id)
            context["data"]=data
    except:
        pass     
    all_products=add_product.objects.all().order_by("product_name")
    context["products"]=all_products
    return render(r,"about.html",context)    
def productpage(request):
    context={'data1':Service_Category.objects.all().order_by("-id"),
              "gallery":gallery.objects.all().order_by("-id"),}
    all_products=add_product.objects.all().order_by("product_name")
    context["products"]=all_products
    if "qry" in request.GET:
       q = request.GET["qry"]
       prd = add_product.objects.filter(product_name__contains=q)
       context["products"] = prd
    if "cat" in request.GET:
       cid=request.GET["cat"]
       prd = add_product.objects.filter(product_category__id=cid)
       context["products"] = prd
    try:
        check=Customer.objects.filter(user__id=request.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=request.user.id)
            context["data"]=data
    except:
        pass  
        #now write
    return render(request,"product.html",context)   



def product12page(r):
    context={'data1':Service_Category.objects.all().order_by("-id"),
              "gallery":gallery.objects.all().order_by("-id"),}
    return render(r,"product12.html",context) 



def gallerypage(r):
    context={ 'data1':Service_Category.objects.all().order_by("-id"),}
    try:
        check=Customer.objects.filter(user__id=r.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=r.user.id)
            context["data"]=data
    except:
        pass      
    ga=gallery.objects.all().order_by('cat_name')
    context["gallery"]=ga
    all_gallery=gallery_data.objects.all()
    context["gallery1"]=all_gallery
    if r.method=="POST":
        searched=r.POST['searched']
        context["searched"]=searched
        ven=gallery_data.objects.filter(detail__contains=searched)
        context["gallery1"]=ven
        return render(r,"gallery.html",context)
    if "gid" in r.GET:
        gid=r.GET['gid']
        ven=gallery_data.objects.filter(thegallery__id=gid)
        context["searched"]=gid
        context["gallery1"]=ven             
    return render(r,"gallery.html",context)



def update_product(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    pid=request.GET["pid"]
    product=Service_Category.objects.get(id=pid)
    context["product"]=product
    if request.method=="POST":
        pr=request.POST["pp"]
        sp=request.POST["sp"]
        de=request.POST["de"]
        product.category=pr
        product.desc=de
        product.total=sp
        if "pimg" in request.FILES:
            img = request.FILES["pimg"]
            product.image = img
        product.save()
        context["status"] = "changes save successfully!!!"
        context["id"] = pid
  
    return render(request,"updateproduct.html",context)

def emptydelete(request):
    # this is for Service_Category delete
    try:
        url=request.META.get('HTTP_REFERER')
        if "pid" in request.GET:
            pid = request.GET["pid"]
            prd = get_object_or_404(Service_Category,id=pid)
            prd.delete()
            return HttpResponseRedirect(url)
        return HttpResponseRedirect(url)
    except:
        pass    
    return redirect('home')    

def add_to_cart(request):
    context={"gallery":gallery.objects.all().order_by("-id"),
             'data1':Service_Category.objects.all().order_by("-id"),
             }
    items=cart.objects.filter(user__id=request.user.id,status=False)
    context["items"]=items 
    if request.user.is_authenticated:
        if request.method=="POST":
            pid =request.POST["pid"]
            qty=request.POST["qty"]  
            is_exist=cart.objects.filter(product__id=pid,user__id=request.user.id,status=False)     
            if len(is_exist)>0:
                context["msz"]="Item Already Exists in Your Menu"
                context["cls"]="alert alert-warning"   
            else:
                product=get_object_or_404(add_product,id=pid)
                usr=get_object_or_404(User,id=request.user.id)
                c=cart(user=usr, product=product,quantity=qty)         
                c.save()
                context["msz"]="{} added in Your Menu".format(product.product_name)
                context["cls"]="alert alert-success"
                
              
    else:
        context["status"]="Plese Login to View your Cart"
        return render(request,"card.html",context)    
    
    try:
        check=Customer.objects.filter(user__id=request.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=request.user.id)
            context["data"]=data
      
    except:
        pass  
    return render(request,"card.html",context)

def get_cart_data(request):
    
    items=cart.objects.filter(user__id=request.user.id,status=False)
    total,quantity=0,0
    for i in items:
        total += float(i.product.product_price)*i.quantity
        quantity +=int(i.quantity)
    res={
        "total":total,"quan":quantity,
    }  
    return JsonResponse(res)    




def change_quan(request):
    if "delete_cart" in request.GET:
        id =request.GET["delete_cart"]
        cart_obj=get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)   


def single_product(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    id=request.GET["pid"]
    obj=add_product.objects.get(id=id)
    context["product"]= obj
    try:
        if user_login:
            check=Customer.objects.filter(user__id=request.user.id)
            if len(check)>0:
                data=Customer.objects.get(user__id=request.user.id)
                context["data"]=data
                return render(request,"singleproduct.html",context)
    except:
        pass


    return render(request,"singleproduct.html",context)
def single_gallery(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    id=request.GET["pid"]
    obj=gallery_data.objects.get(id=id)
    context["product"]= obj
   
     
   
    try:
        comments=Comment1.objects.filter(product_id=id,status='True').order_by("-id")
        context["comments"]= comments 
     
        #     print(i)
        # # print(comments)
        

        check=Customer.objects.filter(user__id=request.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=request.user.id)
            context["data"]=data
    except:
        pass  
    return render(request,"singlegallery.html",context)

def acomment(request,id):
    url=request.META.get('HTTP_REFERER')
    if request.method=="POST":
        form=CommentForm(request.POST)
        if form.is_valid():
            data=Comment1()
            data.subject=form.cleaned_data['subject']
            data.comment=form.cleaned_data['comment']
            data.product_id=id
            current_user=request.user
            data.user_id=current_user.id
            data.status=True
            data.save()
            messages.success(request,"your review has been sent.Thank you for your interest")
            return HttpResponseRedirect(url)
    return HttpResponseRedirect(url)    


def order_page(r):
    context={"gallery":gallery.objects.all().order_by("-id"),
             'data1':Service_Category.objects.all().order_by("-id"),
             }

    items=cart.objects.filter(user__id=r.user.id,status=False)
    context["items"]=items 
    try:
        a=int(r.GET["text1"])
        b=int(r.GET["text2"])
        c=a*b
        context["result"]=c
    except:
        pass
    cats=City.objects.all()
    context["cats"]=cats
    if r.method=='POST':
        nm=r.POST["name"]
        nm2=r.POST["number"]
        nm_id=r.POST["event"]
        nm3=City.objects.get(id=nm_id)
        user=r.user
        nm4=r.POST["time"]
        nm5=r.POST["date"]
        nm6=r.POST["query"]
        nm7=r.POST["kaka"]
        register_order.objects.create(user=user,Name=nm,phone_no=nm2,event_type=nm3,Time_of_event=nm4,Time_of_date=nm5,description=nm6,totalprice=nm7)
        return render(r,"paymentoption.html",context)
    try:
        ch=Customer.objects.filter(user__id=r.user.id)
        if len(ch)>0:
            data=Customer.objects.get(user__id=r.user.id)
            context["data"]=data
    except:
        pass        
    return render(r,"order.html",context)   


def paymentoption(request):
    context={"gallery":gallery.objects.all().order_by("-id"),
             'data1':Service_Category.objects.all().order_by("-id"),
             }
    items=cart.objects.filter(user__id=request.user.id,status=False)
    context["items"]=items 
    ch=Customer.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data=Customer.objects.get(user__id=request.user.id)
        context["data"]=data
    return  render(request,"paymentoption.html",context)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
from django.contrib import messages
def process_payment(request):
    context={}
    items=cart.objects.filter(user_id__id=request.user.id,status=False)
    adderess32=register_order.objects.filter(user_id__id=request.user.id)
    products333=""
    amt=0
    inv="INV1001-"#ye nahi thaa 1 me
    cart_ids=""
    p_ids=""
    register_order_ids=""
    for k in adderess32:
        amt = str(k.totalprice)
        register_order_ids = str(k.id)+","
    for j in items:
        products333 +=str(j.product.product_name)+"/n"
        p_ids +=str(j.product.id)+","
        inv +=str(j.id)#ye nahi thaa 1 me
        cart_ids += str(j.id)+","
        
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': products333,
        'invoice': inv,
       
        'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                              reverse('payment_cancelled')),                       
        
    }
    
    usr = User.objects.get(username=request.user.username)
    ord = Order(cust_id=usr,cart_ids=cart_ids,product_ids=p_ids,register_order_ids=register_order_ids)#ye nahi thaa 1 me
    ord.save()
    ord.invoice_id=str(ord.id)+inv
    ord.save()
    request.session["order_id"] = ord.id  
    form = PayPalPaymentsForm(initial=paypal_dict)
    context["form"]=form
    em=EmailMessage("Paypal:Order Booking userName:{}".format(request.user.username),"The {}  {} is booked order useing paypal ".format(request.user.first_name,request.user.last_name),to=["vaibhavclass90@gmail.com"])
    em.send()
    return render(request, 'process_payment.html',context)

def payment_done(request):
    if "order_id" in request.session:
        order_id=request.session["order_id"]
        ord_obj=get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object=cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    return render(request,"payment_success.html")

def  payment_cancelled(request):
    return render(request,"payment_failed.html") 

def order_history(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    all_orders=[]
    orders=Order.objects.filter(cust_id__id=request.user.id).order_by("-id")
    for order in orders:
        products=[]
        for id in order.product_ids.split(",")[:-1]:
            product=get_object_or_404(add_product,id=id)
            products.append(product)
        for id in order.register_order_ids.split(",")[:-1]:
            vi=[]
            ji=get_object_or_404(register_order,id=id)
            vi.append(ji)
        ord=  {
            "order_id":order.id,
            "products":products,
            "address":vi,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        } 
        all_orders.append(ord)  
        context["order_history"]=all_orders
   
    try:
        context["status"]="Your Order History is Empty "
        check=Customer.objects.filter(user__id=request.user.id)
        if len(check)>0:
            data=Customer.objects.get(user__id=request.user.id)
            context["data"]=data
    except:
        pass

    return render(request,"order_history.html",context)


def my_cust(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),} 
    products=cart.objects.all()#<QuerySet [<cart: admin>, <cart: admin>, <cart: admin>, <cart: admin>, <cart: admin>, <cart: admin>, <cart: df>, <cart: df>]>
    cust=[]
    ids=[]
    for i in products:
        ji=Order.objects.all()
        us={
            "username":i.user.username,
            "first_name":i.user.first_name,
            "last_name":i.user.last_name,
            "email":i.user.email,
        }
        check = Customer.objects.filter(user__id=i.user.id)#2ye hai all usr ki image get krnekeliye 
        if len(check)>0:
            prf=get_object_or_404(Customer,user__id=i.user.id)
            us["image"]=prf.image ##2 yaha tak
            us["contact"]=prf.contact ##2 yaha tak  
            us["city"]=prf.city ##2 yaha tak
        ids.append(i.user.id)
        count=ids.count(i.user.id) 
        if count<2:
            cust.append(us)
    context["customers"]=cust   
    return render(request,"my_cust.html",context)




def orderbook(request):
    if request.user.is_superuser:
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        all_orders=[]
        # orders=Order.objects.filter(cust_id__id=request.user.id)#current usr
        orders=Order.objects.all().order_by("-id")#all user
        for order in orders:
            products=[]
            for id in order.product_ids.split(",")[:-1]:
                product=get_object_or_404(add_product,id=id)
                products.append(product)
            for id in order.register_order_ids.split(",")[:-1]:
                vi=[]
                ji=get_object_or_404(register_order,id=id)
                vi.append(ji)
            ord=  {
                "order_id":order.id,
                "products":products,
                "address":vi,
                "invoice":order.invoice_id,
                "status":order.status,
                "date":order.processed_on,
            } 
            all_orders.append(ord)  
            context["order_history"]=all_orders
        return render(request,"orderbook.html",context)

    else:
        return redirect('homepage')

def acceptorder(request):
    if "pid" in request.GET:
        id =request.GET["pid"]
        cart_obj=get_object_or_404(register_order,id=id)
        cart_obj.status=True
        cart_obj.save()
        return redirect("orderbook")  
def cancleorder(request):
    if "vpid" in request.GET:
        id =request.GET["vpid"]
        cart_obj=get_object_or_404(Order,id=id)
        cart_obj.status=False
        cart_obj.save()
        return redirect("orderbook")  




def all_userorder(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    all_users=Customer.objects.all()
    context["user12"]=all_users
    if "usery" in request.GET:
       q = request.GET["usery"]
       prd = Customer.objects.filter(user__username__contains=q)
       context["user12"] = prd
    return render(request,"all_userorder.html",context)
def single_user(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    id=request.GET["pid"]
    obj=Customer.objects.get(id=id)
    context["user12"]= obj
    return render(request,"single_user.html",context)



def edit_profile(r):
    if r.user.is_superuser: 
        return redirect('homepage')  
    else:
        context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
        check=Customer.objects.filter(user__id=r.user.id)
        if len(check)>0:
          data=Customer.objects.get(user__id=r.user.id)
          context["data"]=data
        if r.method=="POST":
            fn=r.POST["fname"]
            ln=r.POST["lname"]
            em=r.POST["email"]
            con=r.POST["contact"]
            age=r.POST["age"]
            ct=r.POST["city"]
            gen=r.POST["gender"]
            state=r.POST["state"]
            pincode=r.POST["pincode"]
            abt=r.POST["about"]
            usr=User.objects.get(id=r.user.id)
            usr.first_name=fn
            usr.last_name=ln
            usr.email=em
            usr.save()
    
            data.contact=con
            data.age=age
            data.city=ct
            data.gender=gen
            data.address=abt
            data.state=state
            data.pincode=pincode
            data.save()
    
            if "image" in r.FILES:
                img=r.FILES["image"]
                data.image=img
                data.save()
            context["status"]="changes Save Successfully"
        return render(r,"edit_profile.html",context)


def sendemail(request):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    if request.method=="POST":
        rec=request.POST["to"].split(",")
        sub=request.POST["sub"]
        msz=request.POST["msz"]
        try:
           em=EmailMessage(sub,msz,to=rec)
           em.send()  
           context["message"] = "Email sent"
           context["cls"] = "alert-success"
        except:
           context["message"] = "Could not Send,Plese check Internet Connection / Email Address"
           context["cls"] = "alert-danger"
           
    return render(request,"sendemail.html",context)


def change_password(r):
    context={"gallery":gallery.objects.all().order_by("-id"),'data1':Service_Category.objects.all().order_by("-id"),}
    ch=Customer.objects.filter(user__id=r.user.id)
    if len(ch)>0:

        data=Customer.objects.get(user__id=r.user.id)
        context["data"]=data
    if r.method=="POST":
       current=r.POST["cpwd"]
       new_pas=r.POST["npwd"]

       user=User.objects.get(id=r.user.id)
       un=user.username
       check=user.check_password(current)
       if check==True:
          user.set_password(new_pas)
          user.save()
          context["msz"]="Password Change Sucessfully !!!!"
          context["col"]="alert-success"
          user=User.objects.get(username=un)
          login(r,user)
       else:
           context["msz"]="Incurrect Current Password"
           context["col"]="alert-danger"

    return render(r,"change_password.html",context)


def cashondelevery(request):
    context={}
    items=cart.objects.filter(user_id__id=request.user.id,status=False)
    adderess32=register_order.objects.filter(user_id__id=request.user.id)
    products333=""
    amt=0
    inv="INV1001-"#ye nahi thaa 1 me
    # inv=""# 1 
    cart_ids=""
    p_ids=""
    register_order_ids=""
    for k in adderess32:
        amt = str(k.totalprice)
        register_order_ids = str(k.id)+","
    for j in items:
        products333 +=str(j.product.product_name)+"/n"
        p_ids +=str(j.product.id)+","
        inv +=str(j.id)#ye nahi thaa 1 me
        cart_ids += str(j.id)+","
    usr = User.objects.get(username=request.user.username)
    ord = Order(cust_id=usr,cart_ids=cart_ids,product_ids=p_ids,register_order_ids=register_order_ids,status=True)#ye nahi thaa 1 me
    ord.save()
    ord.invoice_id=str(ord.id)+inv
    for i in ord.cart_ids.split(",")[:-1]:
            cart_object=cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    ord.save()
    em=EmailMessage("CASHON:Order Booking userName:{}".format(request.user.username),"The {}  {} is booked order useing CASHON ".format(request.user.first_name,request.user.last_name),to=["vaibhavclass90@gmail.com"])
    em.send()
    return render(request,"cashon.html",context)