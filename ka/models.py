from django.db import models
from django.contrib.auth.models import User

from crum import get_current_user
# Create your models here.
class Customer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    contact = models.CharField(max_length=100, null=True)
    gender=models.CharField(max_length=250,blank=True,default="Male")
    address = models.CharField(max_length=100, null=True)
    image = models.FileField(upload_to="profiles",null=True,blank=True)
    age=models.CharField(max_length=250,null=True,blank=True)  #
    city=models.CharField(max_length=250,null=True,blank=True)
    state=models.TextField(blank=True,null=True)
    pincode=models.TextField(blank=True,null=True)
    def __str__(self):
        return self.user.first_name
class Status(models.Model):
    status = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.status
class City(models.Model):
    city = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.city

class ID_Card(models.Model):
    card = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.card

class Service_Man(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    contact = models.CharField(max_length=100, null=True)
    gender=models.CharField(max_length=250,blank=True,default="Male")
    city = models.CharField(max_length=100, null=True)
    profile_image = models.FileField(null=True)
    address = models.CharField(max_length=100, null=True)
    doj = models.DateField(null=True)
    dob = models.DateField(null=True)
    id_type = models.CharField(max_length=100, null=True)
    service_name = models.CharField(max_length=100, null=True)
    experience = models.CharField(max_length=100, null=True)
    id_card = models.FileField(null=True)
    image = models.FileField(upload_to="profiles",null=True,blank=True)
    added_on =models.DateTimeField(auto_now_add=True,null=True)
    update_on =models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.first_name
class Service_Category(models.Model):
    category = models.CharField(max_length=30, null=True)
    desc = models.TextField()
    image = models.FileField(null=True)
    total=models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.category
class Service(models.Model):
    category = models.ForeignKey(Service_Category,on_delete=models.CASCADE,null=True)
    service = models.ForeignKey(Service_Man, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.service.user.first_name        

class services_function(models.Model):
    service_name=models.CharField(max_length=250,null=True)
    description=models.TextField(null=True)   
    added_on =models.DateTimeField(auto_now_add=True,null=True)
    def __str__(self):
        return self.service_name
class gallery(models.Model):
    cat_name=models.CharField(max_length=250)
    cover_pic=models.FileField(upload_to="media/gallery") 
    description=models.TextField()   
    added_on =models.DateTimeField(auto_now_add=True,null=True)
    def __str__(self):
        return self.cat_name 
             
class v(models.Model):
    description=models.TextField(null=True)         
    def __str__(self):
        return self.description 
class register_order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    Name=models.CharField(max_length=100)        
    phone_no=models.IntegerField()
    event_type=models.ForeignKey(City,on_delete=models.CASCADE)
    Time_of_event=models.CharField(max_length=15)
    Time_of_date=models.CharField(max_length=15,null=True,blank=True)
    date=models.DateField(auto_now=True,null=True,blank=True)
    description=models.TextField()
    totalprice=models.CharField(max_length=200,null=True,blank=True)
    status=models.BooleanField(default=False)


class gallery_data(models.Model):
    thegallery=models.ForeignKey(gallery,on_delete=models.CASCADE)
    image=models.FileField(null=True,upload_to="gallery")
    detail=models.CharField(max_length=106,null=True)
    added_on =models.DateTimeField(auto_now_add=True,null=True)


class add_product(models.Model):
    product_name=models.CharField(max_length=250)
    product_category=models.ForeignKey(Service_Category,on_delete=models.CASCADE)
    product_price=models.CharField(max_length=250)
    OLD_price=models.CharField(max_length=250,null=True,blank=True)
    product_image=models.ImageField(upload_to="products")
    Product_details=models.TextField()
    added_on =models.DateTimeField(auto_now_add=True,null=True)


class cart(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    product=models.ForeignKey(add_product,on_delete=models.CASCADE)
    quantity=models.IntegerField()
    status=models.BooleanField(default=False)
    added_on =models.DateTimeField(auto_now_add=True,null=True,blank=True)
    update_on =models.DateTimeField(auto_now=True,null=True,blank=True)
    
    def __str__(self):
        return self.user.username   
   

     

class Comment1(models.Model):
    STATUS=(
        ('New','New'),
        ('True','True'),
        ('False','False'),
    )
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    product=models.ForeignKey(gallery_data,on_delete=models.CASCADE)
    subject=models.CharField(max_length=50,blank=True)
    comment=models.CharField(max_length=150,blank=True)
    
    status=models.CharField(max_length=10,choices=STATUS,default='New')
    added_on =models.DateTimeField(auto_now_add=True,null=True)
    update_on =models.DateTimeField(auto_now=True,null=True)
    def __str__(self):
        return self.subject
    
class Order(models.Model):
    cust_id=models.ForeignKey(User,on_delete=models.CASCADE)
    cart_ids=models.CharField(max_length=250)
    product_ids=models.CharField(max_length=250)
    register_order_ids=models.CharField(max_length=250)
    invoice_id=models.CharField(max_length=250)
    status=models.BooleanField(default=False)
    processed_on=models.DateTimeField(auto_now_add=True)