# Generated by Django 3.1.5 on 2021-06-30 13:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ka', '0009_auto_20210628_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register_order',
            name='event_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ka.city'),
        ),
    ]
